﻿module Main where

import Control.Concurrent.STM
import Control.Concurrent
import System.IO
import System.Random

---------------------------------------------------------------------------------------

helper :: Group -> IO () -> IO ()
helper group do_task = do
  (in_gate,out_gate) <- joinGroup group
  passGate in_gate
  do_task
  passGate out_gate

meetInStudy :: Int -> IO ()
meetInStudy id = putStr $ "Elf " ++ show id ++ " meeting in the study\n"

deliverToys :: Int -> IO ()
deliverToys id = putStr $ "Reindeer " ++ show id ++ " delivering toys\n"

elf1 group elf_id = helper group $ meetInStudy elf_id
reindeer1 group reindeer_id = helper group $ deliverToys reindeer_id


---------------------------------------------------------------------------------------

data Gate = MkGate Int (TVar Int)

-- создаем новые ворота
newGate :: Int -> STM Gate
newGate n = do
  tv <- newTVar 0
  return $ MkGate n tv

-- проходим сквозь ворота
passGate :: Gate -> IO ()
passGate (MkGate n tv) = atomically $ do
  left <- readTVar tv
  -- check проверяет условие, если оно выполняется, продолжает, 
  -- если нет -- повторяет попытку транзакции, как только изменятся какие-нибудь
  -- из прочитанных переменных
  check $ left > 0 
  writeTVar tv $ left - 1

-- для Санты: опустошаем ворота и ждём, когда все придут
operateGate :: Gate -> IO ()
operateGate (MkGate n tv) = do
  atomically $ writeTVar tv n
  atomically $ do
    left <- readTVar tv
    check $ left == 0

---------------------------------------------------------------------------------------

data Group = MkGroup Int (TVar (Int,Gate,Gate))

-- мы не будет вызывать newGroup внутри STM
newGroup :: Int -> IO Group
newGroup n = atomically $ do
  g1 <- newGate n
  g2 <- newGate n
  tv <- newTVar (n,g1,g2)
  return $ MkGroup n tv

joinGroup :: Group -> IO (Gate,Gate)
joinGroup (MkGroup n tv) = atomically $ do
  (left,g1,g2) <- readTVar tv
  check $ left > 0
  writeTVar tv (left-1, g1,g2)
  return (g1,g2)

awaitGroup :: Group -> STM (Gate,Gate)
awaitGroup (MkGroup n tv) = do
  (left, g1,g2) <- readTVar tv
  check $ left == 0
  new_g1 <- newGate n
  new_g2 <- newGate n
  writeTVar tv (n,new_g1,new_g2)
  return (g1,g2)

---------------------------------------------------------------------------------------

elf gp id = forkIO $ forever $ do
  elf1 gp id
  randomDelay

reindeer gp id = forkIO $ forever $ do
  reindeer1 gp id
  randomDelay

randomDelay :: IO ()
randomDelay = do
  time <- getStdRandom $ randomR (1,1000000)
  threadDelay time

forever :: IO () -> IO ()
forever action = do
  action
  forever action

---------------------------------------------------------------------------------------

santa elf_gp rein_gp = do
  putStr "------"
  choose [ (awaitGroup rein_gp, run "deliver toys")
         , (awaitGroup elf_gp, run "meet for R&D") ]
  where run :: String -> (Gate,Gate) -> IO ()
        run task (in_gate,out_gate) = do
          putStr $ "Ho! Ho! Ho! Let's " ++ task ++ "\n"
          operateGate in_gate
          operateGate out_gate

---------------------------------------------------------------------------------------

choose :: [(STM a, a -> IO ())] -> IO ()
choose choices = do
  act <- atomically (foldr1 orElse actions)
  act
  where actions :: [STM (IO ())]
        actions = [ do val <- guard
                       return $ rhs val
                  | (guard,rhs) <- choices ]

---------------------------------------------------------------------------------------

main = do
  hSetBuffering stdout LineBuffering

  elf_group <- newGroup 3
  sequence_ [ elf elf_group i | i <- [1..10] ]

  reindeer_group <- newGroup 9
  sequence_ [ reindeer reindeer_group i | i <- [1..10] ]

  forever $ santa elf_group reindeer_group


